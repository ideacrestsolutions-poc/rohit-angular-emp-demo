import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AllEmployees , Employee} from 'src/app/Shared/Models/add.emplyee.model';
import { EmployeeService } from 'src/app/Shared/employee.service';
import { SharedService } from '../services/shared.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MessageComponent } from 'src/app/Shared/dialogs/message/message.component';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  addEmployee = new Employee();
  addEmployeeForm : FormGroup;
  constructor(private employeeService: EmployeeService, private router:Router, private dialog: MatDialog) { 

   this.generateAddForm();
  }

  ngOnInit(): void {
  }

  onSubmit(){
    this.employeeService.insertEmployee(this.addEmployeeForm.value).subscribe(
    data=> {
    console.log("employee inserted successfully" + data);
    this.router.navigate(['']);
    }, (err) => {
      console.log("Error Occured",err);
    }, () => {
      this.openDialog();
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(MessageComponent, {
      width: '250px',
      data: { message: "Employee inserted successfully!" }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
  
    private generateAddForm() {

    this.addEmployeeForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      address: new FormControl('', []),
      workLocation: new FormControl('', []),
      position: new FormControl('')
    });

    }

    clearForm() {
      this.addEmployeeForm.reset();
    }
}
