const employeesController = require('../controllers').employees;

module.exports = (app) => {
  app.get('/api', (req, res) => res.status(200).send({ message: 'Welcome to the Todos API!'}));
  app.post('/api/employees', employeesController.create);
  app.get('/api/employees',employeesController.list);
  app.put('/api/employees/:employeeId', employeesController.update);
  app.get('/api/employees/:employeeId',employeesController.retrieve);
  app.get('/api/recentupdatedemployees/',employeesController.listUpdatedEmployee);
};