import { Component, OnInit, EventEmitter, Output, Input, ViewChild } from '@angular/core';
import { Employee } from 'src/app/Shared/Models/add.emplyee.model';
import { EmployeeService } from 'src/app/Shared/employee.service';
import { MatDialog } from '@angular/material/dialog';
import { MessageComponent } from 'src/app/Shared/dialogs/message/message.component';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  @Input() employee = new Employee();
  @Output() submitted = new EventEmitter<any>();
  data = [];
  @ViewChild('employeeForm') employeeForm: NgForm;

  constructor(private employeeService: EmployeeService, private dialog: MatDialog) {
  }

  ngOnInit(): void {
  }

  onSubmit() {
    this.submitted.emit(this.employee);
    this.employee.isRecentUpdate = true;
    this.employee.updateMessage = "Employee updated with Id".concat(String(this.employee.employeeId));
   // this.employee.updateMessage = "Employee Updated";
    this.employeeService.updateEmployee(this.employee.employeeId, this.employee).subscribe(data => {
      this.openDialog(data);
      console.log("employee updated successfully" + data)
    });
  }

  openDialog(data): void {
    const dialogRef = this.dialog.open(MessageComponent, {
      width: '250px',
      data: { message: "Employee updated successfully!" }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.data = result = data;
      this.employeeSubmitted()
    });
  }
  employeeSubmitted() {
    this.submitted.emit(this.data)
  }
 clearForm() {
   this.employeeForm.reset();
 }
}



