import { Component, OnInit, NgZone } from '@angular/core';
import { Location } from '@angular/common';
import { EmployeeService } from 'src/app/Shared/employee.service';

import {  of as observableOf } from 'rxjs';
import { Employee } from 'src/app/Shared/Models/add.emplyee.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  data = [];
  constructor(private location: Location,  private updateService: EmployeeService, private ngZone: NgZone ) { }

  ngOnInit(): void {


  setInterval( () =>
    this.allRecentEmployees()
  ,15000);
  }
  goBack() {
    this.location.back();
  }
 
goForward() {
  this.location.forward();
}

allRecentEmployees() {
  this.updateService.getRecentEmployees().subscribe(data => {
    this.data = data;
  },
    (err) => {
      return observableOf([]);
    },
    () => {
        console.log(this.data.length);
    }
  );
}
}
