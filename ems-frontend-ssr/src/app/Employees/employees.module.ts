import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllEmployeesComponent } from './Tables/all-employees/all-employees.component';
import { EditComponent } from './edit/edit.component';
import { SharedModule } from '../Shared/shared/shared.module';
import { RoutingModule } from './routing/routing.module';
import { AddComponent } from './add/add.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MassEditComponent } from './mass-edit/mass-edit.component';
import { InlineEditComponent } from '../Shared/inline-edit/inline-edit.component';



@NgModule({
  declarations: [AllEmployeesComponent,
    EditComponent,
    AddComponent,
    MassEditComponent,
    InlineEditComponent],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    RoutingModule,
  ]
})
export class EmployeesModule { }
