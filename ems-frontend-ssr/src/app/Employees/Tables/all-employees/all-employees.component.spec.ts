import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AllEmployeesComponent } from './all-employees.component';
import { APP_BASE_HREF } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestModule } from 'src/app/Shared/test/test.module';

describe('AllEmployeesComponent', () => {
  let component: AllEmployeesComponent;
  let fixture: ComponentFixture<AllEmployeesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[TestModule],
      providers:[   { provide: APP_BASE_HREF, useValue : '/' }],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
      declarations: [ AllEmployeesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllEmployeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
