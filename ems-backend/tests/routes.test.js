const request = require('supertest')
const app = require('./../app')

describe('should api Endpoint', () => {
  it('should check api endpoint', async () => {
    const res = await request(app)
      .get('/api')
      .send()
    expect(res.statusCode).toEqual(200)
    expect(res.body).toEqual({"message":'Welcome to the Todos API!'})
  }),

  it('should get List of employess', async () => {
    const res = await request(app)
      .get('/api/employess')
      .send()
    expect(res.statusCode).toEqual(200)
    expect(res.body).not.toBe(null)
  }),

  it('should insert an employee', async () => {
    const res = await request(app)
      .post('/api/employess')
      .send({
        name: 'Rohitashwa Kumar',
        position: 'Software Engineer',
        email: 'rohitashwabgp@gmail.com',
        address: 'Nagwarapalya',
        workLocation: 'Pune'
    })
    expect(res.statusCode).toEqual(201)
    expect(res.body).not.toBe(null)
  }),

  it('should get one employess', async () => {
    const res = await request(app)
      .get('/api/employees')
      .send({employeeId: 1})
    expect(res.statusCode).toEqual(200)
    expect(res.body).not.toBe(null)
  }),

  it('should update an employee', async () => {
    const res = await request(app)
      .put('/api/employees/1')
      .send({employeeId:1} ,{"name": "Rohitashwa Kumar",
        "position": "Software Engineer",
        "email": "rohitashwabgp@gmail.com",
        "address": "Mumbai",
        "workLocation": "Pune"
    })
    expect(res.statusCode).toEqual(200)
    expect(res.body).not.toBe(null)
  })
})
