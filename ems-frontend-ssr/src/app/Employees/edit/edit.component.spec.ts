import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { EditComponent } from './edit.component';
import { TestModule} from './../../Shared/test/test.module';

describe('EditComponent', () => {
  let component: EditComponent;
  let fixture: ComponentFixture<EditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[ TestModule ],
      declarations: [ EditComponent ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form invalid when empty', () => {
    expect(component.employeeForm.valid).toBeFalsy();
  });

  it('email field validity', () => {
    let email = component.employeeForm.controls['email']; 
    expect(email.valid).toBeFalsy(); 
  });

  it('email field validity', () => {
    let errors = {};
    let email = component.employeeForm.controls['email'];
    errors = email.errors || {};
    expect(errors['required']).toBeTruthy(); 
  });

  it('submitting a form & checking validity', () => {
    expect(component.employeeForm.valid).toBeFalsy();
    component.employeeForm.controls['email'].setValue("test@test.com");
    component.employeeForm.controls['name'].setValue("Rohitashwa Kumar");
    expect(component.employeeForm.valid).toBeTruthy();
  });

});

