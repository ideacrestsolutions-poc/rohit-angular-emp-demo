

import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidationErrors } from '@angular/forms';

@Directive({
  selector: '[appEmailValidator]',
  providers: [
    {provide: NG_VALIDATORS, useExisting: EmailValidatorDirective, multi: true}
  ]
})
export class EmailValidatorDirective implements Validator {

  constructor() { 
  }

  validate(control: AbstractControl): ValidationErrors | null {
    const isEmail = control && control.value ? control.value.includes('@'): false;
    if (isEmail) {
      return null;
    } else {
      return { appEmailValidator: false };
    }
  }

}