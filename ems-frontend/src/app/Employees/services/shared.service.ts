import { Injectable, Inject } from '@angular/core';
import { BehaviorSubject } from "rxjs";
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';

const STORAGE_KEY = 'local_employeelist';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

   editingEmployees: any;
  _editingEmployees = new BehaviorSubject<any>(null);

  editedEmployees: any;
  _editedEmployees = new BehaviorSubject<any>(null);


  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) {
    this.editingEmployees;
    this.editedEmployees;
    this._editedEmployees.next(this.editedEmployees);
    this._editingEmployees.next(this._editingEmployees);
  }



 public storeOnLocalStorage(editingEmployee: any): void {    
  this.storage.set(STORAGE_KEY, editingEmployee);
  console.log(this.storage.get(STORAGE_KEY) || 'LocaL storage is empty');
}

public getFromLocalStorage(): BehaviorSubject<any> {       
  const currentEditingList = this.storage.get(STORAGE_KEY) || [];
  this._editingEmployees.next(currentEditingList);
  return this._editingEmployees;
}



}