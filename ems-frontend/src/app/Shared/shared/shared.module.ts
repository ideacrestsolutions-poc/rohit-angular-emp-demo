import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../Material/material.module';
import { HttpClientModule } from '@angular/common/http';
import { EmailValidatorDirective } from '../email-validator.directive';


@NgModule({
  declarations: [EmailValidatorDirective],
  imports: [
    CommonModule,
    HttpClientModule,
    MaterialModule,
    
  ],
  exports :[MaterialModule, EmailValidatorDirective]
})
export class SharedModule { }
