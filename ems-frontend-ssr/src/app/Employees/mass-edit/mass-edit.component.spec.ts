import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MassEditComponent } from './mass-edit.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestModule } from 'src/app/Shared/test/test.module';

describe('MassEditComponent', () => {
  let component: MassEditComponent;
  let fixture: ComponentFixture<MassEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[TestModule],
      declarations: [ MassEditComponent ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MassEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
