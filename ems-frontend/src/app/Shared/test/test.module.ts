import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from '../shared/shared.module';
import { APP_BASE_HREF } from '@angular/common';
@NgModule({
  declarations: [],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot([]),
    SharedModule,
    BrowserAnimationsModule
  ],
  providers:[   { provide: APP_BASE_HREF, useValue : '/' }],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  exports:[FormsModule, ReactiveFormsModule, SharedModule, BrowserAnimationsModule, RouterModule]
})
export class TestModule { }
