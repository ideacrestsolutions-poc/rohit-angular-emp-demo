export class Employee {
    name: string ="";
    email: string="";
    employeeId: number= null;
    address?: string ="";
    workLocation?: string="";
    position?: string="";
    isRecentUpdate?: boolean=false;
    updateMessage?: string="";
}

export class AllEmployees {
    employees: Employee[];
}
