import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SharedService } from '../services/shared.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { EmployeeService } from 'src/app/Shared/employee.service';
import { MatDialog } from '@angular/material/dialog';
import { MessageComponent } from 'src/app/Shared/dialogs/message/message.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mass-edit',
  templateUrl: './mass-edit.component.html',
  styleUrls: ['./mass-edit.component.scss']
})
export class MassEditComponent implements OnInit {

  data = [];
  constructor(private sharedService: SharedService, private updateService: EmployeeService, private dialog: MatDialog, private router: Router) { }

  ngOnInit(): void {
    this.sharedService.getFromLocalStorage().subscribe(data => {
     this.data = data;
    });
  }

  
  massUpdateEmployee() {
    this.data.forEach(row => {
      this.updateService.updateEmployee(row.employeeId,row).subscribe(data=> {
        if(this.data.indexOf(row) === (this.data.length -1) && data) {
            this.openDialog("Employees updated successfully!");
          } 
          this.router.navigate(['']);
      })
     });
  }
  
  setSelectionRange(i,j) {
  console.log(i,j);
  }
  
  openDialog(data): void {
    const dialogRef = this.dialog.open(MessageComponent, {
      width: '250px',
      data: { message: data }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
}
