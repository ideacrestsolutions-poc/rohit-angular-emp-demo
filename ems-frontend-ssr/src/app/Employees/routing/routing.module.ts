import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllEmployeesComponent } from '../Tables/all-employees/all-employees.component';
import { EditComponent } from '../edit/edit.component';
import { MassEditComponent } from '../mass-edit/mass-edit.component';
import { AddComponent } from '../add/add.component';

export const routes: Routes = [
 // { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: '', component: AllEmployeesComponent },
  { path: 'employees/edit', component: EditComponent },
  { path: 'employees/add', component: AddComponent },
  { path: 'employees/massedit', component: MassEditComponent}  
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RoutingModule { }
