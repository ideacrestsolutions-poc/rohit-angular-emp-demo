import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddComponent } from './add.component';

import { TestModule} from './../../Shared/test/test.module';
describe('AddComponent', () => {
  let component: AddComponent;
  let fixture: ComponentFixture<AddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[TestModule],
      declarations: [ AddComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form invalid when empty', () => {
    expect(component.addEmployeeForm.valid).toBeFalsy();
  });

  it('email field validity', () => {
    let email = component.addEmployeeForm.controls['email']; 
    expect(email.valid).toBeFalsy(); 
  });

  it('name field validity', () => {
    let errors = {};
    let name = component.addEmployeeForm.controls['name'];
    errors = name.errors || {};
    expect(errors['required']).toBeTruthy(); 
  });

  it('submitting a form & check validity', () => {
    expect(component.addEmployeeForm.valid).toBeFalsy();
    component.addEmployeeForm.controls['email'].setValue("test@test.com");
    component.addEmployeeForm.controls['name'].setValue("Rohitashwa Kumar");
    expect(component.addEmployeeForm.valid).toBeTruthy();
  });

});
