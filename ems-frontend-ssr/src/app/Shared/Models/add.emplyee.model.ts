export class Employee {
    name: string ="";
    email: string="";
    employeeId: number= null;
    address: string ="";
    workLocation: string="";
    position: string="";
}

export class AllEmployees {
    employees: Employee[];
}
