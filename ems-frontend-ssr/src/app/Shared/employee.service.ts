import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable,of } from 'rxjs';
import { urls } from './../Shared/Models/app.constant';


@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

 
  constructor(private _httpClient: HttpClient) {}

  getEmployees(): Observable<any> {
    //const href = 'assets/employeeData.json';
    return this._httpClient.get<any>(urls.employess);
  }

  updateEmployee(employeeId, emp) : Observable<any> {
    
    return this._httpClient.put<any>(urls.employess.concat(employeeId),emp);
  }

  insertEmployee(employee) {
    return this._httpClient.post<any>(urls.employess,employee);
  }

  getEmployeesById(id): Observable<any> {
    //const href = 'assets/employeeData.json';
    return this._httpClient.get<any>(urls.employess.concat(id));
  }

}
