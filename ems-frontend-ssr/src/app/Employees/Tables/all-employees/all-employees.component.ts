import { Component, ViewChild, OnInit, NgZone } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import {  of as observableOf } from 'rxjs';
import { EmployeeService } from './../../../Shared/employee.service';
import { SelectionModel } from '@angular/cdk/collections';
import { MatTableDataSource } from '@angular/material/table';
import { SharedService } from './../../services/shared.service';
import { Router } from '@angular/router';
import { Employee } from 'src/app/Shared/Models/add.emplyee.model';

@Component({
  selector: 'app-all-employees',
  templateUrl: './all-employees.component.html',
  styleUrls: ['./all-employees.component.scss']
})

export class AllEmployeesComponent implements OnInit {
  displayedColumns: string[] = ['select', 'employeeId',  'name', 'email', 'address', 'workLocation', 'position','actions'];
  dataSource: MatTableDataSource<any> = new MatTableDataSource();
  isLoadingResults = true;
  isError = false;
  data = [];
  selection = new SelectionModel<any>(true, []);
  isEditEmployee = false;
  isMasterUpdate =false;
  isUpdated = false;
  employee = new Employee();
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private employeeService: EmployeeService, private sharedService: SharedService,
    private router: Router, private ngZone: NgZone) {

  }

  ngOnInit() {
    this.allEmployees();
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  allEmployees() {
    this.employeeService.getEmployees().subscribe(data => {
      this.data = data;
    },
      (err) => {
        this.isLoadingResults = false;
        this.isError = true;
        return observableOf([]);
      },
      () => {
          this.isLoadingResults = false;
          this.isError = false;
          this.dataSource = new MatTableDataSource(this.data);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          console.log(this.data);
      }
    );
  }


  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource ? this.dataSource.data.length : null;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected() ? this.selection.clear() : this.dataSource!.data.forEach(row => {
       this.selection.select(row)});
  }

  
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.position + 1}`;
  }

  addEmployee() {
    this.router.navigate(['employees/add']);
  }

  massEditEmployee() {
    this.sharedService.storeOnLocalStorage(this.selection.selected);
    this.router.navigate(['employees/massedit']);
  }

  editEmployee(data) {
    this.isEditEmployee = true;
    this.employee = {... data};
  }

  edit(data) {
    this.isEditEmployee = !this.isEditEmployee;
    this.data.forEach(row => {
    row.employeeId === data.employeeId ? this.data[this.data.indexOf(row)] = data : row =row });
    this.ngZone.run(()=> {
      this.dataSource = new MatTableDataSource(this.data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  }

}


