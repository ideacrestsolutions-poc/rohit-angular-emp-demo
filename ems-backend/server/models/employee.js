'use strict';
module.exports = (sequelize, DataTypes) => {
  const Employee = sequelize.define('Employee', {
    name: {
    type:DataTypes.STRING,
    allowNull: false
  },
    position: {
      type:DataTypes.STRING
    },
    employeeId:{    
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
      isRecentUpdate: {
        type: DataTypes.BOOLEAN
      },
      updateMessage: {
        type: DataTypes.STRING
      },
    email: {type:DataTypes.STRING,
      allowNull: false},
    address: DataTypes.STRING,
    workLocation: DataTypes.STRING
  }, {});
  Employee.associate = function(models) {
  };
  return Employee;
};