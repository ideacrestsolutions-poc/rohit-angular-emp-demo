const Employee = require('../models').Employee;

module.exports = {
  create(req, res) {
    return Employee.create({
        name: req.body.name, position: req.body.position,
        email: req.body.email, address: req.body.address,
        workLocation: req.body.workLocation, isRecentUpdate: req.body.isRecentUpdate,
        updateMessage: req.body.updateMessage
      }).then(employee => res.status(201).send(employee))
        .catch(error => res.status(400).send(error));
  },

  list(req, res) {
    return Employee
      .findAll({
        order: [
          ['createdAt', 'DESC'],
        ],
      })
      .then((employees) => res.status(200).send(employees))
      .catch((error) => res.status(400).send(error));
  },

  listUpdatedEmployee(req, res) {
    return Employee
      .findAll({
        where: { isRecentUpdate: true },
        order: [
          ['createdAt', 'DESC'],
        ],
      })
      .then((employees) => res.status(200).send(employees))
      .catch((error) => res.status(400).send(error));
  },

  retrieve(req, res) {
    console.log(req.params.employeeId);
    return Employee.findOne({ where: { employeeId: req.params.employeeId } }).then(employee => {
      if (!employee) {
        return res.status(404).send({
        message: 'Employee Not Found',
        });
      }
      return res.status(200).send(employee);
    })
    .catch(error => res.status(400).send(error));
  },

  update(req, res) {
    return Employee.findOne({ where: { employeeId: req.params.employeeId } })
    .then(employee => {
      if (!employee) {
        return res.status(404).send({
          message: 'Employee Not Found',
        });
      }
      return employee.update({
        name: req.body.name || employee.name, 
        position: req.body.position || employee.position,
        email: req.body.email || employee.email, 
        address: req.body.address || employee.address,
        isRecentUpdate: req.body.isRecentUpdate  || employee.isRecentUpdate,
        updateMessage: req.body.updateMessage || employee.updateMessage,
        workLocation: req.body.workLocation || employee.workLocation
        })
        .then(() => res.status(200).send(employee)) 
        .catch((error) => res.status(400).send(error));
    })
    .catch((error) => res.status(400).send(error));
  }
};